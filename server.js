// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var cookieParser = require('cookie-parser');
var Hashes = require('jshashes');
var SHA256 =  new Hashes.SHA256;
var SHA512 = new Hashes.SHA512;

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Parse cookie
app.use(cookieParser());

// Serve static files
app.use('/login', express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/", (req, res, next) =>{
	
	if(req.cookies.token){
		var decoded = req.cookies.token;
		if (decoded){
			var user = db.all('SELECT * FROM users WHERE ident=? AND password=?',[decoded.ident, decoded.password], function(err, data){
				if(user.length>=1)
				{
					res.send('Kikou'+decode.ident)
				}	
			});
		}
		else{
			res.redirect('/login');
		}
	}
});

app.post("/login", function(req, res, next) {
	const login = req.body.login;
	const password = req.body.password;
	const sql = "SELECT * FROM users WHERE ident=? AND password=?";
	
	db.all(sql,[login,password], (err, rows) =>{
		if (rows.length > 0){
			var token =  SHA512.hex(SHA512.hex(req.body.login) + SHA512.hex(req.body.password))
			sql1 = "INSERT INTO sessions VALUES(?,?)";
			db.run(sql1, [login,token]);
			res.cookie("token",token);
			res.send({send:true, token});
		}
		else{
			res.send({send:false});
		}
		});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
